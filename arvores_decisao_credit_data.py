#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 20:36:09 2019

@author: maransatto
"""
import pandas as pd

# Recupera base
base = pd.read_csv('credit-data.csv')
base.loc[base.age < 0, 'age'] = 40.92 # Preenche idade com a média

previsores = base.iloc[:, 1:4].values # Recupera previsores
classe = base.iloc[:, 4].values # recupera Classe

# Pré-Processamento - Transforma Atualiza valores nulos (NaN) para a média.
from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0)
imputer = imputer.fit(previsores[:, 1:4])
previsores[:, 1:4] = imputer.transform(previsores[:, 1:4])

# Pré-Processamento - Faz escalonamento
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)

# Separa a base entre treinamento e teste
from sklearn.model_selection import train_test_split
previsores_treinamento, previsores_teste, classe_treinamento, classe_teste = train_test_split(previsores, classe, test_size=0.25, random_state=0)

# Aplica os treinamentos pra gerar o classificador e prediz com as variáveis de teste
from sklearn.tree import DecisionTreeClassifier
classificador = DecisionTreeClassifier(criterion='entropy', random_state=0)
classificador.fit(previsores_treinamento, classe_treinamento) # aqui ele está treinando apenas uma parte da base, para identificar a acurácia testando a outra parte dos mesmos registros
previsoes = classificador.predict(previsores_teste)

# E aqui gera as métricas de matriz de confusão e dá um score para acurácia
# SUPIMPA!! ESTOU APRENDENDO!!!
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(classe_teste, previsoes)
matriz = confusion_matrix(classe_teste, previsoes)