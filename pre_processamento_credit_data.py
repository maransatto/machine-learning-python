#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 22 20:28:09 2019

@author: maransatto
"""

import pandas as pd
base = pd.read_csv('credit-data.csv')
base.describe()

# localizar registros conforme filtro
base.loc[base['age'] < 0]

# 17. TÉCNICAS DE TRATAMENTO DE VALORES INCONSISTENTES
# Apagar coluna
base.drop('age', 1, inplace=True)
# Apagar somente os registros c om problema
base.drop(base[base.age < 0].index, inplace=True)
# preencher os valores manualmente
# preencher os valores com a média
base.mean()
base['age'].mean()
base['age'][base.age > 0].mean()
base.loc[base.age < 0, 'age'] = 40.92;

# 18. TRATAMENTO DE VALORES FALTANTES
pd.isnull(base['age'])
base.loc[pd.isnull(base['age'])]

previsores = base.iloc[:,1:4].values 
classe = base.iloc[:,4].values

from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values='NaN', strategy='mean', axis=0)
imputer = imputer.fit(previsores[:, 0:3])
previsores[:, 0:3] = imputer.transform(previsores[:, 0:3]);

# /Users/maransatto/anaconda3/lib/python3.7/site-packages/sklearn/utils/deprecation.py:58: DeprecationWarning: Class Imputer is deprecated; Imputer was deprecated in version 0.20 and will be removed in 0.22. Import impute.SimpleImputer from sklearn instead.
#  warnings.warn(msg, category=DeprecationWarning)

# 19. ESCALONAMENTO DE ATRIBUTOS - BASE DE CRÉDITO
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)